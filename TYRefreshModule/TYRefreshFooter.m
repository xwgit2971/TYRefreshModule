//
//  TYRefreshFooter.m
//  TYRefreshModule
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYRefreshFooter.h"

@implementation TYRefreshFooter

+ (instancetype)ty_footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock {
    TYRefreshFooter *footer = [[self alloc] init];
    footer.refreshingBlock = refreshingBlock;
    [footer setTitle:@"上拉加载更多" forState:MJRefreshStateIdle];
    [footer setTitle:@"加载更多 ..." forState:MJRefreshStateRefreshing];
    [footer setTitle:@"没有更多数据" forState:MJRefreshStateNoMoreData];
    return footer;
}

@end
