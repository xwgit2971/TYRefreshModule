Pod::Spec.new do |s|
  s.name = 'TYRefreshModule'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'MJRefresh自定义头部和尾部(业务公用组件)'
  s.homepage = 'https://gitlab.com/xwgit2971/TYRefreshModule'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYRefreshModule.git', :tag => s.version }
  s.source_files = 'TYRefreshModule/*.{h,m}'
  s.framework = 'UIKit'
  s.requires_arc = true
  s.dependency  "MJRefresh", '~> 3.1.12'
end
