//
//  ViewController.m
//  TYRefreshModule
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "ViewController.h"
#import "TYRefresh.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
    self.tableView.tableFooterView = [UIView new];

    self.tableView.mj_header = [TYRefreshHeader ty_headerWithRefreshingBlock:^{
        NSLog(@"下拉刷新");
    }];
    self.tableView.mj_footer = [TYRefreshFooter ty_footerWithRefreshingBlock:^{
        NSLog(@"上拉加载更多");
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"row: %ld", indexPath.row+1];
    return cell;
}

@end
