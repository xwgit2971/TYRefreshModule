# TYRefreshModule
MJRefresh自定义头部和尾部(业务公用组件)

## How To Use

```objective-c
#import <TYRefresh/TYRefresh.h>
...
self.tableView.mj_header = [TYRefreshHeader ty_headerWithRefreshingBlock:^{
    NSLog(@"下拉刷新");
}];
self.tableView.mj_footer = [TYRefreshFooter ty_footerWithRefreshingBlock:^{
    NSLog(@"上拉加载更多");
}];
```
