//
//  TYRefreshFooter.h
//  TYRefreshModule
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import <MJRefresh/MJRefresh.h>

@interface TYRefreshFooter : MJRefreshAutoNormalFooter

+ (instancetype)ty_footerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock;

@end
