//
//  TYRefreshHeader.m
//  TYRefreshModule
//
//  Created by 夏伟 on 2016/11/9.
//  Copyright © 2016年 夏伟. All rights reserved.
//

#import "TYRefreshHeader.h"

@implementation TYRefreshHeader

+ (instancetype)ty_headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock {
    TYRefreshHeader *header = [[self alloc] init];
    header.refreshingBlock = refreshingBlock;
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"释放刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"努力加载中 ..." forState:MJRefreshStateRefreshing];
    header.lastUpdatedTimeLabel.hidden = YES;
    return header;
}
@end
